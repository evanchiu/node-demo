// A toy example where we hardcode the data
function getFavorites() {
  return {
    red: 7,
    blue: 5,
    yellow: 10,
    orange:7,
    green: 15 
  };
}

module.exports = getFavorites;
