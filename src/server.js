const http = require("http");
const favorites = require("./favorites")();
const port = process.env.PORT || 3000;

var server = http.createServer(function (req, res) {
  res.writeHead(200, { "Content-Type": "text/plain" });
  res.end(getIndex());
});

// Return a string providing the index page text
function getIndex() {
  // Get list of favorite colors, sorted by popularity
  const favoriteColors = Object.keys(favorites).sort(function (a, b) {
    return favorites[b] - favorites[a];
  });
  // Return text page listing colors with number of votes
  return (
    "Node Demo\n" +
    favoriteColors
      .map((color) => `* ${color} - ${favorites[color]}`)
      .join("\n")
  );
}

server.listen(port);
console.log(`HTTP Server listening on port ${port}`);
