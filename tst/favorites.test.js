const getFavorites = require("../src/favorites");

describe("favorites", () => {
  it("has expected number of votes", () => {
    const voteArray = Object.values(getFavorites());
    const voteCount = voteArray.reduce((acc, current) => acc + current, 0);
    expect(voteCount).toBe(44);
  });

  it("has the expected number of colors", () => {
    expect(Object.keys(getFavorites()).length).toBe(5);
  });
});
